/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

/**
 *
 * @author 0602124
 */
public class Item {
    
    private Integer id;
    private String name;
    private Integer price;
    private String description;

    public Item(Integer id, String name, Integer price, String description) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Integer getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }
    
    public String getFormatedPrice(){
        
        return price/100 + "," + String.format("%02d", price%100);
        
    }
    
}
