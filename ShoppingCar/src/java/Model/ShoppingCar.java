/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author 0602124
 */
public class ShoppingCar {
    
    private static Map<Integer, ItemStock> myItens;
    private static ShoppingCar instance;
    
    private ShoppingCar() {
        myItens = new HashMap();
    }

    public static ShoppingCar getInstance(){
        if(instance == null){
            instance = new ShoppingCar();
        }
        return instance;
    }
    
    public void addItem(Item item, int quantity){
        ItemStock itemStock = myItens.get(item.getId());
        if(quantity > 0){
            if(itemStock==null){
                itemStock = new ItemStock(item, quantity);
                myItens.put(item.getId(), itemStock);
            }else{
                itemStock.setAvaliableQuantity(itemStock.getAvaliableQuantity() + quantity);
            }
        }
    }
    
    public void remItem(Item item){
        myItens.remove(item.getId());
    }
    
    public void setItemQuantity(Item item, int quantity){
        ItemStock itemStock = myItens.get(item.getId());
        if(itemStock!=null){
            itemStock.setAvaliableQuantity(quantity);
        }
    }
    
    public Item getItemById(int id){
        ItemStock itemStock = myItens.get(id);
        return (itemStock==null?null:itemStock.getItem());
    }
    
    public int getItemQuantityById(int id){
        ItemStock itemStock = ShoppingCar.myItens.get(id);
        return (itemStock==null?0:itemStock.getAvaliableQuantity());
    }
    
    public void setItemQuantityById(int id, int quantity){
        ItemStock itemStock = myItens.get(id);
        itemStock.setAvaliableQuantity(quantity);
    }
    
    public Map getAllItens(){
        return myItens;
    }
    
    public void show(){
        
        for (Map.Entry pair : myItens.entrySet()) {
            System.out.println("Item " + ((Integer)pair.getKey()));
            System.out.println(" - Nome:" + ((ItemStock)pair.getValue()).getItem().getName());
            System.out.println(" - Quantidade:" + ((ItemStock)pair.getValue()).getAvaliableQuantity());
        }
    }
    
    
}
