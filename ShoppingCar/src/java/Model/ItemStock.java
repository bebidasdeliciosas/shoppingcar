/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.Map;

/**
 *
 * @author 0602124
 */
public class ItemStock {
    
    Item item;
    int avaliableQuantity;

    public ItemStock(Item item, int avaliableQuantity) {
        this.item = item;
        this.avaliableQuantity = avaliableQuantity;
    }

    public Item getItem() {
        return item;
    }

    public int getAvaliableQuantity() {
        return avaliableQuantity;
    }

    public void setAvaliableQuantity(int avaliableQuantity) {
        this.avaliableQuantity = avaliableQuantity;
    }
    
    public String getFormatedPrice(){
        return this.item.getFormatedPrice();
    }
    
    
    
}
