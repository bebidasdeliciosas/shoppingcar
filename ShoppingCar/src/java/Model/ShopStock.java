/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Igor
 */
public class ShopStock {
    
    private static Map<Integer, ItemStock> stock;
    private static ShopStock instance;
    
    private ShopStock() {
        stock = new HashMap();
        
        Item item1 = new Item(1, "Suco de Laranja", 200, "Suco Integral de Laranja Delicioso");
        Item item2 = new Item(2, "Suco de Groselha", 200, "Suco Integral de Groselha Delicioso");
        Item item3 = new Item(3, "Suco de Tamarindo", 200, "Suco Integral de Tamarindo Delicioso");
        Item item4 = new Item(4, "Vinho Tinto", 750, "Vinho Tinto Integral Delicioso");
        Item item5 = new Item(5, "Vinho Branco", 600, "Vinho Branco Integral Delicioso");
        Item item6 = new Item(6, "Vinho Rosé", 700, "Vinho Rosé Integral Delicioso");
        Item item7 = new Item(7, "Dolly Guaraná", 150, "O Sabor Brasileiro");
        Item item8 = new Item(8, "Guaraná Jesus", 880, "Guaraná divino Delicioso");
        
        stock.put(item1.getId(),  new ItemStock(item1, 10));
        stock.put(item2.getId(),  new ItemStock(item2, 7));
        stock.put(item3.getId(),  new ItemStock(item3, 9));
        stock.put(item4.getId(),  new ItemStock(item4, 3));
        stock.put(item5.getId(),  new ItemStock(item5, 2));
        stock.put(item6.getId(),  new ItemStock(item6, 3));
        stock.put(item7.getId(),  new ItemStock(item7, 5));
        stock.put(item8.getId(),  new ItemStock(item8, 5));
    }

    public static ShopStock getInstance(){
        if(instance == null){
            instance = new ShopStock();
        }
        return instance;
    }
      
    public void addItemStock(ItemStock itemStock){
        ShopStock.stock.put(itemStock.getItem().getId(), itemStock);
    }
    
    public Item getItemById(int id){
        ItemStock itemStock = ShopStock.stock.get(id);
        
        return (itemStock==null?null:itemStock.getItem());
    }
    
    public int getItemQuantityById(int id){
        ItemStock itemStock = ShopStock.stock.get(id);
        return (itemStock==null?0:itemStock.getAvaliableQuantity());
    }
    
    public void setItemQuantityById(int id, int quantity){
        ItemStock itemStock = ShopStock.stock.get(id);
        
        itemStock.setAvaliableQuantity(quantity);
    }
    
    public Map getAllItens(){
        return stock;
    }
    
}
