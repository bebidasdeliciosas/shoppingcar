/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ViewControl;

import Model.ItemStock;
import Model.ShopStock;
import Model.ShoppingCar;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Igor
 */
@WebServlet(name = "MainServlet", urlPatterns = {"/", "/mostruario.php"})
public class MainServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Shopping Car</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Loja de Bebidas Deliciosas</h1>");
            
            
            out.println("<a href='/ShoppingCar/meuCarrinho.php'>Meu Carrinho de Compras</a>");
            
            ShopStock shopStock = ShopStock.getInstance();
            ShoppingCar myShoppingCar = ShoppingCar.getInstance();
            
            if("carAddItem".equals(request.getParameter("sequencePage"))){
                
                String errorMessage = null;
                if(!isNumeric(request.getParameter("itemId")) || shopStock.getItemById(Integer.parseInt(request.getParameter("itemId"))) == null){ 
                    errorMessage = "O item solicitado não está registrado!";
                }else
                if(!isNumeric(request.getParameter("quantity"))){
                    errorMessage = "Quantidade informada não é um valor numérico!";
                }else {
                    int itemId = Integer.parseInt(request.getParameter("itemId"));
                    int itemQuantityInStock = shopStock.getItemQuantityById(itemId);
                    int itemQuantityInCar = myShoppingCar.getItemQuantityById(itemId);
                    int itemQuantityRequested = Integer.parseInt(request.getParameter("quantity"));
                    
                    if(itemQuantityInCar + itemQuantityRequested > itemQuantityInStock){
                        errorMessage = "Não há itens suficiente em estoque para suprir esta demanda!";
                    } 
                }
                
                if(errorMessage != null){
                    out.println("<p style='color:red;'>"+errorMessage+"</p>");
                }else{
                    int itemId = Integer.parseInt(request.getParameter("itemId"));
                    int itemQuantityRequested = Integer.parseInt(request.getParameter("quantity"));
                    
                    myShoppingCar.addItem(shopStock.getItemById(itemId), itemQuantityRequested);
                            
                    out.println("<p style='color:limegreen;'>"+shopStock.getItemById(itemId).getName()+" ×"+itemQuantityRequested+" adicionado ao Carrinho!</p>");
                }
            }
            System.out.println("-------------------");
            myShoppingCar.show();
            out.println("<h2>Mostruario de Bebidas:</h2><ul>");
            
            Iterator it = shopStock.getAllItens().entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                
                out.println("<li><h3>" + 
                    ((ItemStock)pair.getValue()).getItem().getName() + "</h3><p>" +
                    ((ItemStock)pair.getValue()).getItem().getDescription()+ "<br>Valor: R$ " +
                    ((ItemStock)pair.getValue()).getItem().getFormatedPrice()+"</p>"+
                    "<form method='post' action=''><input type=submit value='Adicionar ao Carrinho' ");
                
                int carItemStock = myShoppingCar.getItemQuantityById(((ItemStock)pair.getValue()).getItem().getId());
                if(((ItemStock)pair.getValue()).getAvaliableQuantity() - carItemStock > 0)
                    out.println(">  Quantidade: <input type='number' name='quantity' min='1' max='"+
                        (((ItemStock)pair.getValue()).getAvaliableQuantity() - carItemStock) + 
                        "' value='1'></p><input type='hidden' name='itemId' value='"+pair.getKey()+
                        "'><input type='hidden' name='sequencePage' value='carAddItem'></form></li>");
                else out.println(" disabled>  Sem Itens no Estoque</p></form></li>");
            }
            
            out.println("</ul>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }
    
    public static boolean isNumeric(String str){  
        try{  
            int d = Integer.parseInt(str);  
        }  
        catch(NumberFormatException nfe){  
            return false;  
        }  
        return true;  
    }
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
