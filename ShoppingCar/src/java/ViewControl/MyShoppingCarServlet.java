/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ViewControl;

import Model.ItemStock;
import Model.ShopStock;
import Model.ShoppingCar;
import static ViewControl.MainServlet.isNumeric;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Igor
 */
@WebServlet(name = "MyShoppingCarServlet", urlPatterns = {"/meuCarrinho.php"})
public class MyShoppingCarServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Shopping Car</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Loja de Bebidas Deliciosas</h1>");
            
            out.println("<a href='/ShoppingCar/mostruario.php'>Mostruário de Bebidas</a>");
            
            ShopStock shopStock = ShopStock.getInstance();
            ShoppingCar myShoppingCar = ShoppingCar.getInstance();
            
            if("setQuantity".equals(request.getParameter("sequencePage"))){
                
                String errorMessage = null;
                if(!isNumeric(request.getParameter("itemId")) || shopStock.getItemById(Integer.parseInt(request.getParameter("itemId"))) == null){ 
                    errorMessage = "O item solicitado não está registrado!";
                }else
                if(!isNumeric(request.getParameter("quantity"))){
                    errorMessage = "Quantidade informada não é um valor numérico!";
                }else 
                if(Integer.parseInt(request.getParameter("quantity")) <= 0) {
                    errorMessage = "Quantidade informada é menor ou igual a zero!";
                }else {
                    int itemId = Integer.parseInt(request.getParameter("itemId"));
                    int itemQuantityInStock = shopStock.getItemQuantityById(itemId);
                    int itemQuantityRequested = Integer.parseInt(request.getParameter("quantity"));
                    
                    if(itemQuantityRequested > itemQuantityInStock){
                        errorMessage = "Não há itens suficiente em estoque para suprir esta demanda!";
                    }
                }
                
                if(errorMessage != null){
                    out.println("<p style='color:red;'>"+errorMessage+"</p>");
                }else{
                    int itemId = Integer.parseInt(request.getParameter("itemId"));
                    int itemQuantityRequested = Integer.parseInt(request.getParameter("quantity"));
                    
                    myShoppingCar.setItemQuantity(shopStock.getItemById(itemId), itemQuantityRequested);
                    
                    out.println("<p style='color:limegreen;'>"+shopStock.getItemById(itemId).getName()+" alterado para "+itemQuantityRequested+" itens!</p>");
                    
                }
                
            }else
            if("removeItem".equals(request.getParameter("sequencePage"))){
                
                String errorMessage = null;
                if(!isNumeric(request.getParameter("itemId")) || shopStock.getItemById(Integer.parseInt(request.getParameter("itemId"))) == null){ 
                    errorMessage = "O item solicitado não está registrado!";
                }
                
                if(errorMessage != null){
                    out.println("<p style='color:red;'>"+errorMessage+"</p>");
                }else{
                    int itemId = Integer.parseInt(request.getParameter("itemId"));
                    
                    myShoppingCar.remItem(shopStock.getItemById(itemId));
                    
                    out.println("<p style='color:limegreen;'>"+shopStock.getItemById(itemId).getName()+" removido do carrinho!</p>");
                    
                }
                
            }else
            if("checkout".equals(request.getParameter("sequencePage"))){
               
                Iterator it = myShoppingCar.getAllItens().entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    ItemStock carItemStock = (ItemStock)pair.getValue();
                    
                    int itemQuantityInStock = shopStock.getItemQuantityById(carItemStock.getItem().getId());
                    int itemQuantityRequested = carItemStock.getAvaliableQuantity();
                    
                    shopStock.setItemQuantityById(carItemStock.getItem().getId(), itemQuantityInStock - itemQuantityRequested);
                    //myShoppingCar.remItem(carItemStock.getItem());
                }
                myShoppingCar.getAllItens().clear();
                out.println("<p style='color:limegreen;'>Compra concluída com sucesso!</p>");
                    
            }
                
           
            out.println("<h2>Meu Carrinho de Bebidas:</h2><ul>");
            
            int quotation = 0;
            Iterator it = myShoppingCar.getAllItens().entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                
                out.println("<li><h3>" + 
                    ((ItemStock)pair.getValue()).getItem().getName() + "</h3><p>" +
                    ((ItemStock)pair.getValue()).getItem().getDescription()+ "<br>Valor: R$ " +
                    ((ItemStock)pair.getValue()).getItem().getFormatedPrice()+"</p>"+
                    //Form Alterar Quantidade
                    "<form method='post' action=''><input type=submit value='Alterar Quantidade'>  Quantidade: <input type='number' name='quantity'  min='1' max='"+(shopStock.getItemQuantityById((Integer)pair.getKey()))
                    +"' value='"+((ItemStock)pair.getValue()).getAvaliableQuantity() + 
                    "'><input type='hidden' name='itemId' value='"+pair.getKey()+
                    "'><input type='hidden' name='sequencePage' value='setQuantity'></form><br>"+
                    //Form Remove Item
                    "<form method='post' action=''><input type=submit value='Remover Item'><input type='hidden' name='itemId' value='"+pair.getKey()+
                    "'><input type='hidden' name='sequencePage' value='removeItem'></form></li>");
                
                quotation += ((ItemStock)pair.getValue()).getItem().getPrice() * ((ItemStock)pair.getValue()).getAvaliableQuantity();
            }
            
            out.println("</ul>");
            
            out.println("<h2>Valor Total: R$ "+quotation/100 + "," + String.format("%02d", quotation%100)+"</h2>");
            out.println("<form method='post' action=''><input type=submit value='Finalizar Compra'><input type='hidden' name='sequencePage' value='checkout'></form>");
            
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
